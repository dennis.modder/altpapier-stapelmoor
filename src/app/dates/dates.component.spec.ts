import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatesComponent } from './dates.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

describe('DatesComponent', () => {
  let component: DatesComponent;
  let fixture: ComponentFixture<DatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [DatesComponent],
    imports: [],
    providers: [provideHttpClient(withInterceptorsFromDi())]
})
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
