import { Component, OnInit } from '@angular/core';
import {DatesService} from '../dates.service';

@Component({
    selector: 'app-dates',
    templateUrl: './dates.component.html',
    styleUrls: ['./dates.component.scss'],
    standalone: false
})
export class DatesComponent implements OnInit {

  public dates: Date[] | undefined;

  constructor(private dateService: DatesService) {
  }

  ngOnInit(): void {
    this.dateService.datesSubject.subscribe((dates) => {
      const y: Date|null = this.dateService.getNextDate();
      if (y){
        this.dates = dates[y.getFullYear()];
      }
    });
  }
}
