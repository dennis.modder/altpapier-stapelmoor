import {Component, OnInit} from '@angular/core';
import {DatesService} from '../dates.service';
import {InfoDialogComponent} from '../info-dialog/info-dialog.component';
import {TextElement} from '../text-element';
import {MatDialog} from "@angular/material/dialog";

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss'],
    standalone: false
})
export class InfoComponent implements OnInit {

  public nextDate: Date | null = null;
  public checks: TextElement[];
  public warnings: TextElement[];

  constructor(private dateService: DatesService, private dialog: MatDialog) {
    this.checks = [
      {
        title: 'Zeiten',
        description: 'Die Abgabe des Altpapiers ist von 9:00 Uhr - 12:00 Uhr am Sammeltag möglich.'
      },
      {
        title: 'Standort',
        description: 'Der Container befindet sich beim Jugendheim der ev.-ref. Kirchengemeinde (Alter Schulweg)',
        link: {
          text: 'Karte anzeigen',
          destination: 'https://goo.gl/maps/CJnPPEi2w8U9eP9B9',
          target: '_blank'
        }
      },
      {
        title: 'Zweck',
        description: 'Der Erlös der Sammlung kommt der Jugendarbeit in der Kirchengemeinde und der Jugendfeuerwehr zugute.'
      }
    ];
    this.warnings = [];
  }

  ngOnInit(): void {
    this.dateService.datesSubject.subscribe(() => {
      this.nextDate = this.dateService.getNextDate();
    });
  }

  public showContent(item: TextElement){
    this.dialog.open(InfoDialogComponent, {
      data: item,
    });
  }

}
