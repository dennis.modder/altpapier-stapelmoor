import {Component, Inject, OnInit} from '@angular/core';
import {google, outlook, office365, yahoo, ics} from 'calendar-link';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {CalendarEvent} from 'calendar-link/dist/interfaces';
import {MatDialog} from "@angular/material/dialog";

@Component({
    selector: 'app-date-card-dialog',
    templateUrl: './date-card-dialog.component.html',
    styleUrls: ['./date-card-dialog.component.scss'],
    standalone: false
})
export class DateCardDialogComponent implements OnInit {

  public google: SafeUrl | undefined;
  public outlook: SafeUrl | undefined;
  public office365: SafeUrl | undefined;
  public yahoo: SafeUrl | undefined;
  public ics: SafeUrl | undefined;

  /**
   * Constructor
   *
   * @param sanitizer
   * @param dialog
   * @param bottomSheetRef
   * @param data
   */
  constructor(private sanitizer: DomSanitizer,
              private dialog: MatDialog,
              private bottomSheetRef: MatBottomSheetRef<DateCardDialogComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) private data: Date) {
  }

  /**
   * onInit
   */
  ngOnInit(): void {
    if (this.data) {
      const start = new Date(this.data);
      const end = new Date(this.data);

      start.setHours(start.getHours() + 9);
      end.setHours(end.getHours() + 12);

      const event: CalendarEvent = {
        title: 'Altpapiersammlung',
        description: 'Altpapiersammlung in Stapelmoor. ' +
          'Annahme beim Jugendheim der Kirchengemeinde zwischen 09:00 Uhr und 12:00 Uhr.',
        start: start.toISOString(),
        end: end.toISOString(),
        location: 'Alter Schulweg 3, 26826 Weener'
      };

      this.google = this.sanitizer.bypassSecurityTrustUrl(google(event));
      this.outlook = this.sanitizer.bypassSecurityTrustUrl(outlook(event));
      this.office365 = this.sanitizer.bypassSecurityTrustUrl(office365(event));
      this.yahoo = this.sanitizer.bypassSecurityTrustUrl(yahoo(event));
      this.ics = this.sanitizer.bypassSecurityTrustUrl(ics(event));
    }
  }

  /**
   * handles onClick
   */
  public onClickBottomSheetItem() {
    this.bottomSheetRef.dismiss();
  }
}
