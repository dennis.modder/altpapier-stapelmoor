import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DateCardDialogComponent} from './date-card-dialog.component';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetModule, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MatListModule} from "@angular/material/list";
import {MAT_DIALOG_DATA, MatDialogModule} from "@angular/material/dialog";

describe('DateCardDialogComponent', () => {
  let component: DateCardDialogComponent;
  let fixture: ComponentFixture<DateCardDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatBottomSheetModule,
        MatListModule,
        MatDialogModule
      ],
      declarations: [DateCardDialogComponent],
      providers: [
        {
          provide: MatBottomSheetRef,
          useValue: {}
        },
        {
          provide: MAT_BOTTOM_SHEET_DATA,
          useValue: new Date()
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateCardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
