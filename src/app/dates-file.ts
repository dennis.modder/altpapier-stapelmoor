export interface DatesFile {
  [key: string]: Record<string, Date | number | null>;
}
