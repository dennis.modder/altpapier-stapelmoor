import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavigationComponent} from './navigation/navigation.component';
import {DatesComponent} from './dates/dates.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {DateCardComponent} from './date-card/date-card.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {InfoComponent} from './info/info.component';
import {FaqComponent} from './faq/faq.component';
import {DateDiffPipe} from './date-diff.pipe';
import {ContactComponent} from './contact/contact.component';
import {MatLineModule, MatRippleModule} from '@angular/material/core';
import {InfoDialogComponent} from './info-dialog/info-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FooterComponent} from './footer/footer.component';
import {DateCardDialogComponent} from './date-card-dialog/date-card-dialog.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatListModule} from "@angular/material/list";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";

@NgModule({ declarations: [
        AppComponent,
        NavigationComponent,
        DatesComponent,
        DateCardComponent,
        InfoComponent,
        FaqComponent,
        DateDiffPipe,
        ContactComponent,
        InfoDialogComponent,
        FooterComponent,
        DateCardDialogComponent
    ],
    bootstrap: [AppComponent], imports: [BrowserModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatListModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatRippleModule,
    MatBottomSheetModule, MatLineModule
  ], providers: [
        { provide: LOCALE_ID, useValue: 'de' },
        provideHttpClient(withInterceptorsFromDi())
    ] })
export class AppModule {
}
