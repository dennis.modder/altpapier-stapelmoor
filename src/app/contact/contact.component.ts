import {Component} from '@angular/core';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    standalone: false
})
export class ContactComponent  {

  public sendMail(){
    const emailAddress = 'info@feuerwehr-stapelmoor.de';
    window.location.href = `mailto:${emailAddress}?subject=Altpapier`;
  }

}
