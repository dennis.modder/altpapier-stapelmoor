import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dateDiff',
    standalone: false
})
export class DateDiffPipe implements PipeTransform {

  transform(days: number, ...args: unknown[]): string {
    if (days > 0){
      if (days === 1){
        return 'heute';
      } else if (days === 2) {
        return 'morgen';
      } else if (days >= 28) {
        return 'in ' + Math.ceil(days/7) + ' Wochen';
      } else {
        return 'in ' + days + ' Tagen';
      }
    }
    return 'bereits stattgefunden';
  }

}
