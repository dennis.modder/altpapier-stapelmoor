import {Component, Inject, OnInit} from '@angular/core';
import {TextElement} from '../text-element';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
    selector: 'app-info-dialog',
    templateUrl: './info-dialog.component.html',
    styleUrls: ['./info-dialog.component.scss'],
    standalone: false
})
export class InfoDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public content: TextElement ) {}

  ngOnInit(): void {
  }

}
