import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    standalone: false
})
export class NavigationComponent implements OnInit {

  public shareable = false;

  constructor() { }

  ngOnInit(): void {
    // @ts-ignore
    if (window.navigator && navigator.share) {
      this.shareable = true;
    }
  }

  public share(){
    if (this.shareable) {
      navigator.share({
        title: 'Altpapier in Stapelmoor',
        text: 'Sammeltermine und Hinweise',
        url: window.location.href,
      })
        .then(() => console.log('Successful share'))
        .catch((error) => console.log('Error sharing', error));
    }
  }

}
