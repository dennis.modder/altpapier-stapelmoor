export interface TextElement {
  title: string;
  description: string;
  link?: {
    text: string;
    destination: string;
    target: string;
  };
}
