import {Component} from '@angular/core';
import {SwUpdate} from '@angular/service-worker';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    standalone: false
})
export class AppComponent {

  public year: number;

  constructor(private swUpdate: SwUpdate, private snackBar: MatSnackBar) {
    this.year = new Date().getFullYear();

    this.swUpdate.versionUpdates.subscribe(event => {
      switch (event.type) {
        case 'VERSION_DETECTED':
          console.log(`Downloading new app version: ${event.version.hash}`);
          break;
        case 'VERSION_READY':
          console.log(`Current app version: ${event.currentVersion.hash}`);
          console.log(`New app version ready for use: ${event.latestVersion.hash}`);
          this.showUpdateNotification();
          break;
        case 'VERSION_INSTALLATION_FAILED':
          console.log(`Failed to install app version '${event.version.hash}': ${event.error}`);
          break;
      }
    });

    this.swUpdate.unrecoverable.subscribe(event => {
      const ref = this.snackBar.open('Es ist ein Fehler aufgetreten (' + event.reason + ')', 'Neuladen', {
        duration: 10000,
      });
      ref.onAction().subscribe(() => {
        document.location.reload();
      });
    });
  }

  private showUpdateNotification(){
    const ref = this.snackBar.open('Eine neue Version steht bereit.', 'Neuladen', {
      duration: 10000,
    });
    ref.onAction().subscribe(() => {
      this.swUpdate.activateUpdate().then(() => document.location.reload());
    });
  }
}
