import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../environments/environment';
import {DatesFile} from './dates-file';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatesService {

  public dates: Record<number, Date[]> = {};
  public datesSubject: Subject<Record<number, Date[]>> = new Subject<Record<number, Date[]>>();

  /**
   * Constructor
   *
   * @param http
   */
  constructor(private http: HttpClient) {
    this.getDatesFromFile();
  }

  /**
   * Returns next date
   */
  public getNextDate(): Date | null {
    const now: Date = new Date();
    if (this.dates[now.getFullYear()].length > 0) {
      for (const date of this.dates[now.getFullYear()]) {
        if (date > now) {
          return date;
        }
      }
      return this.dates[now.getFullYear() + 1][0];
    }
    return null;
  }


  /**
   * Read dates from file
   *
   * @private
   */
  private getDatesFromFile() {
    this.http.get<DatesFile>(environment.datesFile).subscribe((data: DatesFile) => {
      for (const year of Object.keys(data)) {
        if (Object.keys(data[year]).length > 0) {
          const dates: Date[] = [];
          for (const month of Object.keys(data[year])) {
            const day = data[year][month];
            if (typeof day === 'number') {
              dates.push(new Date(Number(year), Number(month), day));
            }
          }
          this.dates[Number(year)] = dates;
        }
      }
      this.datesSubject.next(this.dates);
    });
  }
}
