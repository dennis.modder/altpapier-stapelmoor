import {Component, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {DateCardDialogComponent} from '../date-card-dialog/date-card-dialog.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';

@Component({
    selector: 'app-date-card',
    templateUrl: './date-card.component.html',
    styleUrls: ['./date-card.component.scss'],
    standalone: false
})
export class DateCardComponent implements OnInit {

  /**
   * Date input
   */
  @Input() date: Date | undefined;

  /**
   * Event is forthcoming
   */
  public isForthcoming = false;

  /**
   * Constructor
   *
   * @param bottomSheet
   * @param renderer
   * @param elementRef
   */
  constructor(
    private bottomSheet: MatBottomSheet,
    private renderer: Renderer2,
    private elementRef: ElementRef) {
  }

  ngOnInit(): void {
    if (this.date) {
      this.isForthcoming = (this.diff(this.date) > 0);
    }
    if (this.isForthcoming) {
      this.renderer.addClass(this.elementRef.nativeElement, 'forthcoming');
    }
  }

  /**
   * Returns difference between target date and now
   *
   * @param targetDate
   */
  public diff(targetDate: Date): number {
    const offset = 12 * 3600 * 1000; // 12:00
    const diff: number = (targetDate.getTime() + offset) - new Date().getTime();
    if (diff > 0) {
      return Math.ceil(diff / (1000 * 60 * 60 * 24));
    }
    return 0;
  }

  /**
   * Shows calendar dialog
   */
  public showCalDialog() {
    if (this.isForthcoming) {
      this.bottomSheet.open(DateCardDialogComponent, {
        data: this.date,
      });
    }
  }

}
