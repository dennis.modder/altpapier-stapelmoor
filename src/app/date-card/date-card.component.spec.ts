import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateCardComponent } from './date-card.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatCardModule} from "@angular/material/card";

describe('DateCardComponent', () => {
  let component: DateCardComponent;
  let fixture: ComponentFixture<DateCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatBottomSheetModule,
        MatCardModule
      ],
      declarations: [ DateCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should mark as forthcoming', () => {
    const tomorrow = new Date();
    tomorrow.setHours(tomorrow.getHours() + 24);
    component.date = tomorrow;
    component.ngOnInit();
    expect(component.isForthcoming).toBe(true);
  });

  it('should return diff between dates', () => {
    const yesterday = new Date();
    const now = new Date();
    const tomorrow = new Date();
    yesterday.setHours(yesterday.getHours() - 24);
    tomorrow.setHours(tomorrow.getHours() + 24);
    expect(component.diff(yesterday)).toBe(0);
    expect(component.diff(now)).toBe(1);
    expect(component.diff(tomorrow)).toBe(2);
  });
});
