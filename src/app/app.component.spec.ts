import {TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import {ServiceWorkerModule} from '@angular/service-worker';
import {MatDividerModule} from '@angular/material/divider';
import {FooterComponent} from './footer/footer.component';
import {ContactComponent} from './contact/contact.component';
import {FaqComponent} from './faq/faq.component';
import {InfoComponent} from './info/info.component';
import {NavigationComponent} from './navigation/navigation.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DatesComponent} from './dates/dates.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatDialogModule, MatDialogRef} from "@angular/material/dialog";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [
        AppComponent,
        FooterComponent,
        ContactComponent,
        FaqComponent,
        InfoComponent,
        NavigationComponent,
        DatesComponent
    ],
    imports: [BrowserAnimationsModule,
        MatSnackBarModule,
        MatDividerModule,
        MatDialogModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: false })],
    providers: [
        {
            provide: MatDialogRef,
            useValue: {}
        },
        provideHttpClient(withInterceptorsFromDi()),
    ]
}).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
