import { DateDiffPipe } from './date-diff.pipe';

describe('DateDiffPipe', () => {
  it('create an instance', () => {
    const pipe = new DateDiffPipe();
    expect(pipe).toBeTruthy();
  });

  it('transform difference to text', () => {
    const pipe = new DateDiffPipe();
    expect(pipe.transform(1)).toBe('heute');
    expect(pipe.transform(2)).toBe('morgen');
    expect(pipe.transform(3)).toBe('in 3 Tagen');
    expect(pipe.transform(27)).toBe('in 27 Tagen');
    expect(pipe.transform(28)).toBe('in 4 Wochen');
    expect(pipe.transform(0)).toBe('bereits stattgefunden');
    expect(pipe.transform(-1)).toBe('bereits stattgefunden');
  });
});
