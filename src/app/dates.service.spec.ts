import { TestBed } from '@angular/core/testing';

import { DatesService } from './dates.service';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

describe('DatesService', () => {
  let service: DatesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [],
    providers: [provideHttpClient(withInterceptorsFromDi())]
});
    service = TestBed.inject(DatesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
