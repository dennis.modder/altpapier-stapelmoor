import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public isActive: Subject<boolean> = new Subject();

  constructor() {
  }

  public async enableNotification() {

  }

  public disableNotification() {

  }
}
