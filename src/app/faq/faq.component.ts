import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss'],
    standalone: false
})
export class FaqComponent implements OnInit {

  public faq: { title: string; description: string }[];

  constructor() {
    this.faq = [
      {
        title: 'Was muss ich beachten?',
        description: 'Wir nehmen ausschließlich Altpapier, bevorzugt gebündelt, in Paketen oder ' +
          'alternativ in Kunststoffsäcken verpackt, an. Bitte achten Sie darauf, ' +
          'dass Ihre Abgabe kein Restmüll, Kunststoffe oder gefährliche Gegenstände beinhaltet.'
      },
      {
        title: 'Kann ich Altpapier nach 12:00 Uhr abgeben?',
        description: 'Die Abgabe von Altpapier nach 12:00 Uhr ist nicht möglich. ' +
          'Die abgestellten Container sind außerhalb des angegebenen Zeitraums ' +
          'verschlossen und werden nach dem Sammeln zeitnah abtransportiert.'
      },
      {
        title: 'Wird trotz der Papiertonnen gesammelt?',
        description: 'Ja, wir werden auch weiterhin das Altpapier am Container in Stapelmoor annehmen.'
      },
      {
        title: 'Was geschieht mit dem Altpapier?',
        description: 'Das gesammelte Altpapier wird durch ein lokales Unternehmen vollständig recycelt.'
      },
      {
        title: 'Kostet mir die Entsorgung etwas?',
        description: 'Nein. Wir sammeln Ihr Altpapier kostenlos.'
      }
    ];
  }

  ngOnInit(): void {
  }

}
